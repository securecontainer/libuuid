# Overview:

The libuuid library is used to generate unique identifiers for objects that may be accessible beyond the local system. The Linux implementation was created to uniquely identify ext2 filesystems created by a machine. This library generates UUIDs compatible with those created by the Open Software Foundation (OSF) Distributed Computing Environment (DCE) utility uuidgen.

## Homepage

[libuuid project](https://sourceforge.net/projects/libuuid/)

## Notifications (updates):

## Changes done:

## TODO:

